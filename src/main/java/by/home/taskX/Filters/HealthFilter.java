package by.home.taskX.Filters;

import by.home.taskX.Utils.ClusterHealthList;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Check the cluster health through the ClusterHealthList class which ping all the nodes per timeout.
 * If cluster isn't working returns the message and status-code 522.
 */
public class HealthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (ClusterHealthList.getInstance().checkClusterState()) {
            chain.doFilter(request, response);
        } else {
            ((HttpServletResponse) response).setStatus(522);
            response.getWriter().print("Cluster is not working.");
        }
    }

    @Override
    public void destroy() {
    }
}
