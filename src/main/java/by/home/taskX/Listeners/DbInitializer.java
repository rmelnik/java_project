package by.home.taskX.Listeners;

import by.home.taskX.Utils.ConnectionPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Initialises the DB with initial script.
 */
public final class DbInitializer implements ServletContextListener {
    private static final Logger LOGGER = LogManager.getLogger(DbInitializer.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        BufferedReader reader = null;
        Connection c = null;
        try {
            reader = new BufferedReader(new FileReader(sce.getServletContext().getRealPath("/") + "\\WEB-INF\\classes\\script.sql"));
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            c = ConnectionPool.getInstance().retrieve();
            c.prepareStatement(stringBuilder.toString()).execute();
        } catch (FileNotFoundException e) {
            LOGGER.warn("Cannot find the creation script file." + e);
        } catch (IOException e) {
            LOGGER.warn("Cannot read from the creation script file." + e);
        } catch (SQLException e) {
            LOGGER.warn("Cannot execute the script." + e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.warn("Cannot close the reader." + e);
                }
            }
            try {
                ConnectionPool.getInstance().putback(c);
            } catch (Exception e) {
                LOGGER.warn("Cannot put back the connection to connection pool.");
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
