package by.home.taskX.Listeners;

import by.home.taskX.JMS.Server;
import by.home.taskX.Utils.ClusterHealthList;
import by.home.taskX.Utils.ConnectionPool;
import by.home.taskX.Utils.NodeList;
import by.home.taskX.Utils.Rebalancer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Initializes all needed tools for working.
 */
public class ContextListener implements ServletContextListener {
    private static final Logger LOGGER = LogManager.getLogger(ContextListener.class);
    private static Server server;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ClusterHealthList.setIsWorking(true);
        server = new Server();
        NodeList.getInstance();
        ClusterHealthList.getInstance();
        try {
            ConnectionPool.getInstance();
        } catch (IOException e) {
            LOGGER.warn("Can't get access to the credentials file." + e);
        } catch (SQLException e) {
            LOGGER.warn("Can't instantiate the connection pool." + e);
        }
        LOGGER.info("Listener started.");
        new Thread(() -> {
            LOGGER.info("Waiting till all nodes startup for starting rebalance.");
            synchronized (this) {
                while (!ClusterHealthList.getInstance().checkClusterState()) {
                    try {
                        wait(1500);
                    } catch (InterruptedException e) {
                        LOGGER.warn("Waiting for nodes start caught InterruptedException.");
                    }
                }
            }
            new Rebalancer().rebalance();
        }).start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ClusterHealthList.setIsWorking(false);
        server.shutdown();
        try {
            ConnectionPool.getInstance().closeAllConnections();
        } catch (Exception e) {
            LOGGER.warn("Can't close the connections in connection pool." + e);
        }
    }
}
