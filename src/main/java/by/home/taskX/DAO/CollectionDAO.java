package by.home.taskX.DAO;

import by.home.taskX.Entities.CollectionsEntity;
import by.home.taskX.Proxies.AclLog;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface CollectionDAO {
    /**
     * Add the new collection to the DB.
     *
     * @param c
     * @throws SQLException
     */
    @AclLog
    void create(CollectionsEntity c) throws SQLException, IOException;

    /**
     * Returns the collection which name equals the specified.
     *
     * @param name
     * @return
     * @throws SQLException
     */
    @AclLog
    CollectionsEntity read(String name) throws SQLException, IOException;

    /**
     * Returns the few entities(number specified in limit param.) starting from offset position.
     *
     * @return
     * @throws SQLException
     */
    @AclLog
    List<CollectionsEntity> read(int limit, int offset) throws SQLException, IOException;

    /**
     * Change the collection in the DB by it's id.
     *
     * @param col
     * @throws SQLException
     */
    @AclLog
    void update(CollectionsEntity col, String oldName) throws SQLException, IOException;

    /**
     * Delete the collection by it's name.
     *
     * @param name
     * @throws SQLException
     */
    @AclLog
    void delete(String name) throws SQLException, IOException;
}
