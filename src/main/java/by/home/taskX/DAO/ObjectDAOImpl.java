package by.home.taskX.DAO;

import by.home.taskX.Entities.DBEntity;
import by.home.taskX.Entities.NodeConfiguration;
import by.home.taskX.Utils.ConnectionPool;
import by.home.taskX.Utils.NodeList;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ObjectDAOImpl implements ObjectDAO {
    private final String tableName;
    private final Class<? extends DBEntity> clazz;

    /**
     * Assigns the type of the generic DBEntity object.
     * If DB doesn't have table for current entity. It will creates a new one.
     */
    public ObjectDAOImpl(Class<? extends DBEntity> clazz) throws SQLException, IOException {
        this.clazz = clazz;
        tableName = clazz.getSimpleName();
    }

    public void create(DBEntity t) throws SQLException, IOException {
        if (t.getId() == null) {
            t.setId(UUID.randomUUID().toString());
        }
        PreparedStatement statement = null;
        Connection connection = retrieveConnection();
        try {
            String sql = "INSERT INTO " + tableName + " Values(?,?);";
            statement = connection.prepareStatement(sql);
            statement.setString(1, t.getId());
            statement.setString(2, t.getValue());
            statement.execute();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    public DBEntity read(String id) throws SQLException, InstantiationException, IllegalAccessException, IOException {
        PreparedStatement statement = null;
        String sql =
                "SELECT * FROM " + tableName +
                        " WHERE ID = ? ";
        DBEntity result;
        Connection connection = ConnectionPool.getInstance().retrieve();
        ResultSet rs = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            rs = statement.executeQuery();
            rs.next();
            result = parseEntity(rs);
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (rs != null) {
                rs.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
        return result;
    }

    public DBEntity read(int number) throws SQLException, InstantiationException, IllegalAccessException, IOException {
        Connection connection = retrieveConnection();
        PreparedStatement statement = null;
        DBEntity result;
        ResultSet rs = null;
        try {
            String sql = "SELECT TOP 1 * FROM (SELECT TOP ? * FROM " + tableName + " ORDER BY ID ASC) ORDER BY ID DESC;";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, number);
            rs = statement.executeQuery();
            rs.next();
            result = parseEntity(rs);
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (rs != null) {
                rs.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
        return result;
    }

    public List<DBEntity> read(int limit, int page) throws SQLException, InstantiationException, IllegalAccessException, IOException {
        Connection connection = retrieveConnection();
        List<DBEntity> list = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            statement = connection.prepareStatement("SELECT * FROM " + tableName + " ORDER BY ID ASC LIMIT ? OFFSET ? ;");
            int lowBorder = limit * (page - 1);
            int upBorder = limit * page;
            List<Integer> stepList = new ArrayList<>();
            List<NodeConfiguration> originList = NodeList.getInstance().getOriginList();
            int groupIndexer = 0;
            String curGroup = NodeList.getInstance().getCurrentNode().getGroup();
            for (int i = 0; i < originList.size(); i++) {
                if (originList.get(i).getGroup().equals(curGroup)) {
                    groupIndexer = i;
                    if (groupIndexer == 0) {
                        groupIndexer = originList.size();
                    }
                    if (groupIndexer == originList.size()) {
                        groupIndexer = 0;
                    }
                }
            }
            int step = groupIndexer;
            while (step < upBorder) {
                stepList.add(step);
                step += originList.size();
            }
            int sqlLimit = 0;
            for (int i = 0; i < stepList.size(); i++) {
                if (stepList.get(i) >= lowBorder && stepList.get(i) <= upBorder) {
                    sqlLimit++;
                }
            }
            int sqlOffset = lowBorder / originList.size();
            statement.setInt(1, sqlLimit);
            statement.setInt(2, sqlOffset);
            rs = statement.executeQuery();
            while (rs.next()) {
                list.add(parseEntity(rs));
            }
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (rs != null) {
                rs.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
        return list;
    }

    public void update(DBEntity entity) throws SQLException, IOException {
        Connection connection = retrieveConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(
                    "UPDATE " + tableName + " SET value=? WHERE id=? ;");
            statement.setString(1, entity.getValue());
            statement.setString(2, entity.getId());
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    public void delete(String id) throws SQLException, IOException {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = retrieveConnection();
            statement = connection.prepareStatement(
                    "DELETE FROM " + tableName + " WHERE id=?");
            statement.setString(1, id);
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    private DBEntity parseEntity(ResultSet rs) throws SQLException, IllegalAccessException, InstantiationException {
        DBEntity t = clazz.newInstance();
        String id = rs.getString(1);
        String value = rs.getString(2);
        t.setId(id);
        t.setValue(value);
        return t;
    }

    public int getRecordsCount() throws SQLException, IOException {
        Connection connection = null;
        Statement ps = null;
        ResultSet rs = null;
        Integer count;
        try {
            connection = retrieveConnection();
            ps = connection.createStatement();
            rs = ps.executeQuery("SELECT COUNT(*) FROM " + tableName);
            rs.next();
            count = rs.getInt(1);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
        return count;
    }

    /**
     * Creates the table if needed.
     *
     * @throws IOException
     * @throws SQLException
     */
    public void initTable() throws IOException, SQLException {
        Connection connection = retrieveConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + tableName + " (`id` VARCHAR(255) PRIMARY KEY,`value` VARCHAR(255) NOT NULL );");
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    public Connection retrieveConnection() throws IOException, SQLException {
        return ConnectionPool.getInstance().retrieve();
    }
}
