package by.home.taskX.DAO;

import by.home.taskX.Entities.CollectionsEntity;
import by.home.taskX.Utils.ConnectionPool;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionsDAOImpl implements CollectionDAO {

    public CollectionsDAOImpl() throws SQLException, IOException {
        Connection connection = ConnectionPool.getInstance().retrieve();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS COLLECTIONS (" +
                    "`name` VARCHAR(255) PRIMARY KEY," +
                    "`type` VARCHAR(3) NOT NULL," +
                    "`maxCount` INTEGER NOT NULL," +
                    "`jsonSchema` VARCHAR(500) NOT NULL);");
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    public void create(CollectionsEntity c) throws SQLException, IOException {
        Connection connection = ConnectionPool.getInstance().retrieve();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(
                    "INSERT INTO COLLECTIONS VALUES (?,?,?,?);");
            statement.setString(1, c.getName());
            statement.setString(2, c.getType());
            statement.setInt(3, c.getMaxCount());
            statement.setString(4, c.getJsonSchema());
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    public CollectionsEntity read(String name) throws SQLException, IOException {
        Connection connection = ConnectionPool.getInstance().retrieve();
        PreparedStatement statement = null;
        CollectionsEntity entity;
        ResultSet rs = null;
        try {
            statement = connection.prepareStatement("SELECT * FROM COLLECTIONS WHERE NAME=?;");
            statement.setString(1, name);
            rs = statement.executeQuery();
            rs.next();
            entity = parseCollection(rs);
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (rs != null) {
                rs.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
        return entity;
    }

    public List<CollectionsEntity> read(int limit, int offset) throws SQLException, IOException {
        Connection connection = ConnectionPool.getInstance().retrieve();
        PreparedStatement statement = null;
        List<CollectionsEntity> list = new ArrayList<>();
        ResultSet rs = null;
        try {
            statement = connection.prepareStatement("SELECT * FROM COLLECTIONS LIMIT ? OFFSET ? ;");
            statement.setInt(1, limit);
            statement.setInt(2, limit * (offset - 1));
            rs = statement.executeQuery();
            while (rs.next()) {
                list.add(parseCollection(rs));
            }
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (rs != null) {
                rs.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
        return list;
    }

    public void update(CollectionsEntity col, String oldName) throws SQLException, IOException {
        Connection connection = ConnectionPool.getInstance().retrieve();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(
                    "UPDATE COLLECTIONS SET NAME=? ,TYPE=?,MAXCOUNT=? WHERE NAME=?;");
            statement.setString(1, col.getName());
            statement.setString(2, col.getType());
            statement.setInt(3, col.getMaxCount());
            statement.setString(4, oldName);
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    public void delete(String name) throws SQLException, IOException {
        Connection connection = ConnectionPool.getInstance().retrieve();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("DELETE FROM COLLECTIONS WHERE NAME=?;");
            statement.setString(1, name);
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
            ConnectionPool.getInstance().putback(connection);
        }
    }

    private CollectionsEntity parseCollection(ResultSet rs) throws SQLException {
        CollectionsEntity c = new CollectionsEntity();
        c.setName(rs.getString(1));
        c.setType(rs.getString(2));
        c.setMaxCount(rs.getInt(3));
        c.setJsonSchema(rs.getString(4));
        return c;
    }

}
