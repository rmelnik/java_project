package by.home.taskX.DAO;

import by.home.taskX.Entities.DBEntity;
import by.home.taskX.Proxies.AclLog;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * interface with common behaviour for partially-generic dao.
 */
public interface ObjectDAO{
    /**
     * Adds entity to the DB which table has the same name as the classname of current entity.
     *
     * @param t
     * @throws SQLException
     */
    @AclLog
    void create(DBEntity t) throws SQLException, IOException;

    /**
     * Returns all the entities in DB.
     *
     * @return
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @AclLog
    List<DBEntity> read(int limit, int offset) throws SQLException, InstantiationException, IllegalAccessException, IOException;

    /**
     * Returns the entity with specified id.
     *
     * @param id
     * @return
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @AclLog
    DBEntity read(String id) throws SQLException, InstantiationException, IllegalAccessException, IOException;

    /**
     * Updates the entity with the same id that current entity id.
     *
     * @param entity
     * @throws SQLException
     */
    @AclLog
    void update(DBEntity entity) throws SQLException, IOException;

    /**
     * Deletes the entity with the specified id.
     *
     * @param id
     * @throws SQLException
     */
    @AclLog
    void delete(String id) throws SQLException, IOException;

}

