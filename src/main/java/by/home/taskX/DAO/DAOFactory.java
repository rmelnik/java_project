package by.home.taskX.DAO;

import java.io.IOException;
import java.sql.SQLException;

public class DAOFactory {

    public DAOFactory() {
    }

    public Object getDAO(DAOType type, Class clazz) throws SQLException, IOException {
        switch (type) {
            case OBJECT:
                return new ObjectDAOImpl(clazz);
            case COLLECTION:
                return new CollectionsDAOImpl();
            default:
                throw new IllegalArgumentException();
        }
    }

    public enum DAOType {
        COLLECTION, OBJECT
    }
}
