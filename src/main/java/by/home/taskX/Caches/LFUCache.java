package by.home.taskX.Caches;

import by.home.taskX.Entities.DBEntity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Cache which free it's space by removing the least frequently used objects.
 */
public class LFUCache implements Cache {

    private int initialCapacity = 10;

    private final LinkedHashMap<String, CacheEntry> cacheMap = new LinkedHashMap<>();

    public LFUCache(int initialCapacity) {
        if (initialCapacity <= 0)
            throw new IllegalArgumentException("Initial capacity cannot be less than 1.");
        this.initialCapacity = initialCapacity;
    }

    public void addEntity(DBEntity entity) {
        if (!isFull()) {
            CacheEntry temp = new CacheEntry();
            temp.setEntity(entity);
            temp.frequency = 0;
            cacheMap.put(entity.getId(), temp);
        } else {
            String entryKeyToBeRemoved = getLFUKey();
            cacheMap.remove(entryKeyToBeRemoved);

            CacheEntry temp = new CacheEntry();
            temp.setEntity(entity);
            temp.frequency = 0;

            cacheMap.put(entity.getId(), temp);
        }
    }

    public Map getMap() {
        return cacheMap;
    }

    private String getLFUKey() {
        String key = null;
        int minFreq = Integer.MAX_VALUE;

        for (Map.Entry<String, CacheEntry> entry : cacheMap.entrySet()) {
            if (minFreq > entry.getValue().frequency) {
                key = entry.getValue().entity.getId();
                minFreq = entry.getValue().frequency;
            }
        }
        return key;
    }

    public DBEntity getEntity(String id) {
        if (cacheMap.containsKey(id)) {
            CacheEntry temp = cacheMap.get(id);
            temp.frequency++;
            cacheMap.put(id, temp);
            return temp.entity;
        }
        return null;
    }

    private boolean isFull() {
        return cacheMap.size() == initialCapacity;
    }

    class CacheEntry {
        private DBEntity entity;
        private int frequency;

        private CacheEntry() {
        }

        void setEntity(DBEntity entity) {
            this.entity = entity;
        }

    }
}
