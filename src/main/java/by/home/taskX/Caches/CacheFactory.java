package by.home.taskX.Caches;

import by.home.taskX.Proxies.CacheHandler;

import java.lang.reflect.Proxy;

public class CacheFactory {
    private static final String LFU="LFU";
    private static final String LRU="LRU";
    private static final String PROXY_LFU="PROXY_LFU";
    private static final String PROXY_LRU="PROXY_LRU";

    public Cache getCache(String type,int maxCount){
        Cache cache;
        switch (type){
            case LFU:
                return new LFUCache(maxCount);
            case LRU:
                return new LRUCache(maxCount);
            case PROXY_LFU:
                cache=new LFUCache(maxCount);
                return (Cache) Proxy.newProxyInstance(cache.getClass().getClassLoader(),
                        cache.getClass().getInterfaces(),
                        new CacheHandler(cache));
            case PROXY_LRU:
                cache=new LRUCache(maxCount);
                return (Cache) Proxy.newProxyInstance(cache.getClass().getClassLoader(),
                        cache.getClass().getInterfaces(),
                        new CacheHandler(cache));
            default:
                throw new IllegalArgumentException("Wrong type of cache.");
        }
    }
}
