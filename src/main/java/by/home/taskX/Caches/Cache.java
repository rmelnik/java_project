package by.home.taskX.Caches;

import by.home.taskX.Entities.DBEntity;

import java.util.Map;

/**
 * Interface that describes the common behaviour of the caches in this project.
 */
public interface Cache {
    /**
     * Put the DBEntity inside the cache.
     *
     * @param entity
     */
    void addEntity(DBEntity entity);

    /**
     * Get the entity by name.
     *
     * @param name
     * @return
     */
    DBEntity getEntity(String name);

    /**
     * returns the Map which encapsulated inside the cache.
     *
     * @return
     */
    Map getMap();

}
