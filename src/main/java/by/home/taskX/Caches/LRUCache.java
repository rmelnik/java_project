package by.home.taskX.Caches;

import by.home.taskX.Entities.DBEntity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Cache which free it's space by removing the least recently used objects.
 */
public class LRUCache implements Cache {
    private final int capacity;
    private static final float LOAD_FACTOR = 0.75f;
    private final Map<String, DBEntity> lruCache;

    public LRUCache(int initialCapacity) {
        if (initialCapacity > 0) {
            this.capacity = initialCapacity;
        } else {
            capacity = 10;
        }
        lruCache = new LinkedHashMap<String, DBEntity>(this.capacity, LOAD_FACTOR, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<String, DBEntity> eldest) {
                return size() > capacity;
            }
        };
    }


    public void addEntity(DBEntity entity) {
        lruCache.put(entity.getId(), entity);
    }

    public Map getMap() {
        return lruCache;
    }

    public DBEntity getEntity(String name) {
        return lruCache.get(name);
    }

}
