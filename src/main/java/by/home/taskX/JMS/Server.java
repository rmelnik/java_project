package by.home.taskX.JMS;

import by.home.taskX.DAO.DAOFactory;
import by.home.taskX.DAO.ObjectDAO;
import by.home.taskX.DAO.CollectionsDAOImpl;
import by.home.taskX.Entities.CollectionsEntity;
import by.home.taskX.Entities.DBEntity;
import by.home.taskX.Utils.NodeList;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.jms.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * This class implements the MessageListener interface, it means that he listening the messages from the broker,
 * parses the input messages, doing some operations accordingly input message,
 * and sends the result of the operations as JMSBuilder formatted string.
 */
public class Server implements MessageListener {
    private static final Logger LOGGER = LogManager.getLogger(Server.class);
    private static final String PREFIX = "by.training.taskX.Entities.";
    private static int ackMode;
    private static String messageQueueName;
    private Session session;
    private boolean transacted = false;
    private MessageProducer replyProducer;
    private ObjectMapper mapper;
    private String cachedObject;

    static {
        messageQueueName = "client.messages" + NodeList.getInstance().getPort();
        ackMode = Session.AUTO_ACKNOWLEDGE;
    }

    public Server() {
        this.setupMessageQueueConsumer();
        mapper = new ObjectMapper();
        BrokerService broker = new BrokerService();
        broker.setPersistent(false);
        broker.setUseJmx(false);
        try {
            broker.addConnector(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
            broker.start();
        } catch (Exception e) {
            LOGGER.warn("Couldn't start the local broker service." + e);
        }
    }

    /**
     * Setup the current listener which will wait for messages.
     */
    private void setupMessageQueueConsumer() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        Connection connection;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            this.session = connection.createSession(this.transacted, ackMode);
            Destination adminQueue = this.session.createQueue(messageQueueName);
            this.replyProducer = this.session.createProducer(null);
            this.replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            MessageConsumer consumer = this.session.createConsumer(adminQueue);
            consumer.setMessageListener(this);
        } catch (JMSException e) {
            LOGGER.warn("Failed to setup the server messages consumer.");
        }
    }

    /**
     * Invokes when listener caught the new message from queue.
     *
     * @param message
     */
    public void onMessage(Message message) {
        String messageText = null;
        try {
            TextMessage response = this.session.createTextMessage();
            response.setJMSReplyTo(session.createQueue(messageQueueName));
            if (message instanceof TextMessage) {
                TextMessage txtMsg = (TextMessage) message;
                messageText = txtMsg.getText();
                LOGGER.info(NodeList.getInstance().getPort() + ";Handling the:" + messageText);
                response.setJMSCorrelationID(message.getJMSCorrelationID());
                String[] splittedMessage = messageText.split(";");
                if (splittedMessage.length > 1 && !splittedMessage[JMSMessageBuilder.METHOD_INDEX].equals("GET")) {
                    cachedObject = createRollbackedMessage(message);
                }
                if (messageText.equals("ROLLBACK") && cachedObject != null) {
                    LOGGER.warn("Rolling back the:" + cachedObject);
                    handleRequest(cachedObject, null);
                    response.setText(messageState.COMMITTED.name());
                    cachedObject = null;
                } else if (messageText.equals("COMMIT")) {
                    commitMessage();
                    LOGGER.info("Send the \"COMMITED\" back.");
                    response.setText(messageState.COMMITTED.name());
                } else {
                    handleRequest(messageText, response);
                }
                this.replyProducer.send(message.getJMSReplyTo(), response);
            }
        } catch (IOException e) {
            LOGGER.warn("Caught the IOException. While processing:" + messageText + "; Exception cause:" + e);
        } catch (InstantiationException e) {
            LOGGER.warn("Caught the InstantiationException. While processing:" + messageText + "; Exception cause:" + e);
        } catch (SQLException e) {
            LOGGER.warn("Caught the SQLException. While processing:" + messageText + "; Exception cause:" + e);
        } catch (IllegalAccessException e) {
            LOGGER.warn("Caught the IllegalAccessException. While processing:" + messageText + "; Exception cause:" + e);
        } catch (ClassNotFoundException e) {
            LOGGER.warn("Caught the ClassNotFoundException. While processing:" + messageText + "; Exception cause:" + e);
        } catch (JMSException e) {
            LOGGER.warn("Caught the JMSException. While processing:" + messageText + "; Exception cause:" + e);
        }
    }

    /**
     * Parses request by it's method type.
     *
     * @param message  input message.
     * @param response response for remote node.
     * @throws JMSException if had some troubles with sending the error to the initial node.
     */
    private void handleRequest(String message, TextMessage response) throws JMSException {
        try {
            String[] splittedMessage = message.split(";");
            switch (splittedMessage[JMSMessageBuilder.METHOD_INDEX]) {
                case "GET":
                    response.setText(handleGet(splittedMessage));
                    break;
                case "POST":
                    handlePost(splittedMessage);
                    response.setText("Ok");
                    break;
                case "PUT":
                    handlePut(splittedMessage);
                    break;
                case "DELETE":
                    handleDelete(splittedMessage);
                    break;
            }
            if (response != null) {
                response.setText(messageState.STAGED.name());
            }
        } catch (IOException e) {
            LOGGER.warn("IOException was thrown while handling the :" + message);
            response.setText("Error;IO");
        } catch (SQLException e) {
            LOGGER.warn("SQLException was thrown while handling the :" + message);
            response.setText("Error;SQL");
        } catch (ClassNotFoundException e) {
            LOGGER.warn("ClassNotFoundException was thrown while handling the :" + message);
            response.setText("Error;ClassNotFound");
        } catch (IllegalAccessException e) {
            LOGGER.warn("IllegalAccessException was thrown while handling the :" + message);
            response.setText("Error;IllegalAccess");
        } catch (InstantiationException e) {
            LOGGER.warn("InstantiationException was thrown while handling the :" + message);
            response.setText("Error;Instantiation");
        } catch (JMSException e) {
            LOGGER.warn("JMSException was thrown while handling the :" + message);
            response.setText("Error;JMS");
        }
    }

    /**
     * Writes the result of search the requested object for initial node.
     *
     * @param splittedMessage
     * @return the list of entities which satisfy the conditions of request.
     * @throws ClassNotFoundException if trying to get the not existing entity.
     * @throws IOException            when had some troubles with DAO connection pool.
     * @throws SQLException           when had some troubles with DB interaction.
     * @throws IllegalAccessException when try to get access to unavailable resource.
     * @throws InstantiationException when had some troubles with creating the explicit entity.
     */
    private String handleGet(String[] splittedMessage) throws ClassNotFoundException, IOException, SQLException, IllegalAccessException, InstantiationException {
        ObjectDAO objectDAO = getObjectDAO(splittedMessage);
        String result = "";
        if (splittedMessage.length <= JMSMessageBuilder.MESSAGE_INDEX + 1) {
            DBEntity entity = objectDAO.read(splittedMessage[JMSMessageBuilder.MESSAGE_INDEX]);
            result = entity.toString();
        } else {
            int limit = Integer.parseInt(splittedMessage[JMSMessageBuilder.MESSAGE_INDEX]);
            int page = Integer.parseInt(splittedMessage[JMSMessageBuilder.GET_ALL_PAGE_INDEX]);
            List<DBEntity> list = objectDAO.read(limit, page);
            if (list.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (DBEntity e : list) {
                    sb.append(e).append("/");
                }
                int length = sb.toString().length();
                result = sb.toString().substring(0, length - 1);
            }
        }
        return result;
    }

    /**
     * Add the new entity to the DB.
     *
     * @param splittedMessage
     * @return the list of entities which satisfy the conditions of request.
     * @throws ClassNotFoundException if trying to get the not existing entity.
     * @throws IOException            when had some troubles with DAO connection pool.
     * @throws SQLException           when had some troubles with DB interaction.
     */
    private void handlePost(String[] splittedMessage) throws ClassNotFoundException, IOException, SQLException {
        String jsonString = splittedMessage[JMSMessageBuilder.MESSAGE_INDEX];
        if (isObjectMessage(splittedMessage)) {
            Class clazz = Class.forName("by.training.taskX.Entities." + splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]);
            DBEntity entity = (DBEntity) mapper.readValue(jsonString, clazz);
            getObjectDAO(splittedMessage).create(entity);
        } else {
            CollectionsEntity collection = mapper.readValue(jsonString, CollectionsEntity.class);
            getCollectionDAO(splittedMessage).create(collection);
        }
    }

    /**
     * Updates the existing entity in DB.
     *
     * @param splittedMessage
     * @throws ClassNotFoundException if trying to get the not existing entity.
     * @throws IOException            when had some troubles with DAO connection pool.
     * @throws SQLException           when had some troubles with DB interaction.
     */
    private void handlePut(String[] splittedMessage) throws ClassNotFoundException, IOException, SQLException {
        String jsonString = splittedMessage[JMSMessageBuilder.MESSAGE_INDEX];
        if (isObjectMessage(splittedMessage)) {
            Class clazz = Class.forName(PREFIX + splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]);
            DBEntity entity = (DBEntity) mapper.readValue(jsonString, clazz);
            getObjectDAO(splittedMessage).update(entity);
        } else {
            CollectionsEntity collection = mapper.readValue(jsonString, CollectionsEntity.class);
            getCollectionDAO(splittedMessage).update(collection, collection.getName());
        }
    }

    /**
     * Deletes the specified entity from DB.
     *
     * @param splittedMessage
     * @throws ClassNotFoundException if trying to get the not existing entity.
     * @throws IOException            when had some troubles with DAO connection pool.
     * @throws SQLException           when had some troubles with DB interaction.
     */
    private void handleDelete(String[] splittedMessage) throws ClassNotFoundException, IOException, SQLException {
        if (isObjectMessage(splittedMessage)) {
            getObjectDAO(splittedMessage).delete(splittedMessage[JMSMessageBuilder.MESSAGE_INDEX]);
        } else {
            getCollectionDAO(splittedMessage).delete(splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]);
        }
    }

    private ObjectDAO getObjectDAO(String[] splittedMessage) throws ClassNotFoundException, IOException, SQLException {
        return (ObjectDAO) new DAOFactory().getDAO(DAOFactory.DAOType.OBJECT, Class.forName("by.training.taskX.Entities." + splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]));
    }

    private CollectionsDAOImpl getCollectionDAO(String[] splittedMessage) throws ClassNotFoundException, IOException, SQLException {
        return (CollectionsDAOImpl) new DAOFactory().getDAO(DAOFactory.DAOType.COLLECTION, Class.forName("by.training.taskX.Entities." + splittedMessage[1]));
    }

    private boolean isObjectMessage(String[] splittedMessage) {
        return splittedMessage[JMSMessageBuilder.TYPE_INDEX].equals(JMSMessageBuilder.EntityType.OBJECT.name());
    }

    private void commitMessage() {
        cachedObject = null;
    }

    private String createRollbackedMessage(Message message) throws JMSException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, IOException {
        String[] splittedMessage = ((TextMessage) message).getText().split(";");
        ObjectDAO objectDAO = getObjectDAO(splittedMessage);
        JMSMessageBuilder.EntityType type;
        String entityName = splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX];
        JMSMessageBuilder.MethodType method = null;
        String messageString = null;
        Class clazz = Class.forName(PREFIX + splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]);
        boolean isCollection = false;
        if (splittedMessage[JMSMessageBuilder.TYPE_INDEX].equals(JMSMessageBuilder.EntityType.COLLECTION)) {
            type = JMSMessageBuilder.EntityType.COLLECTION;
            isCollection = true;
        } else {
            type = JMSMessageBuilder.EntityType.OBJECT;
        }
        switch (splittedMessage[JMSMessageBuilder.METHOD_INDEX]) {
            case "GET":
                throw new IllegalArgumentException("Get cannot be rollbacked.");
            case "POST":
                method = JMSMessageBuilder.MethodType.DELETE;
                if (isCollection) {
                    messageString = new CollectionsDAOImpl().read(splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]).toString();
                } else {
                    DBEntity tempEntity = (DBEntity) new ObjectMapper().readValue(splittedMessage[JMSMessageBuilder.MESSAGE_INDEX], clazz);
                    messageString = tempEntity.toString();
                }
                break;
            case "PUT":
                method = JMSMessageBuilder.MethodType.PUT;
                if (isCollection) {
                    messageString = new CollectionsDAOImpl().read(splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]).toString();
                } else {
                    DBEntity tempEntity = (DBEntity) new ObjectMapper().readValue(splittedMessage[JMSMessageBuilder.MESSAGE_INDEX], clazz);
                    messageString = objectDAO.read(tempEntity.getId()).toString();
                }
                break;
            case "DELETE":
                method = JMSMessageBuilder.MethodType.PUT;
                if (isCollection) {
                    messageString = new CollectionsDAOImpl().read(splittedMessage[JMSMessageBuilder.ENTITY_NAME_INDEX]).toString();
                } else {
                    DBEntity tempEntity = objectDAO.read(splittedMessage[JMSMessageBuilder.MESSAGE_INDEX]);
                    messageString = objectDAO.read(tempEntity.getId()).toString();
                }
                break;
        }
        return new JMSMessageBuilder().createMessage(type, entityName, method, messageString);
    }

    public void shutdown() {
    }

    public enum messageState {
        STAGED, COMMITTED
    }
}
