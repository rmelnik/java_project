package by.home.taskX.JMS;

import by.home.taskX.Utils.CustomHttpServletRequestWrapper;
import by.home.taskX.Utils.ServletUtils;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.stream.Collectors;

/**
 * This util-class helps to create the valid messages for sending it to remote nodes,
 * so them could handle it and do some work.
 */
public class JMSMessageBuilder {
    private ServletUtils servletUtils = new ServletUtils();
    public static final int TYPE_INDEX = 0;
    public static final int ENTITY_NAME_INDEX = 1;
    public static final int METHOD_INDEX = 2;
    public static final int MESSAGE_INDEX = 3;
    public static final int GET_ALL_PAGE_INDEX = 4;
    private static final int NAME_INDEX_IN_URL = 5;
    private static final int ID_INDEX_IN_URL = 6;

    public enum EntityType {
        COLLECTION, OBJECT
    }

    public enum MethodType {
        GET, POST, PUT, DELETE
    }

    public String createMessage(EntityType type, String entityName, MethodType method, String message) {
        return String.valueOf(type) + ";" + entityName + ";" + method + ";" + message;
    }

    /**
     * Parses the input HttpRequest body, and create message based on it for JMS interaction.
     *
     * @param type entity type object or collection.
     * @param req  input http request.
     * @return JMS message in String.
     * @throws ClassNotFoundException if tries to create not existing entity(inside current system) from the request.
     * @throws SQLException           if had some troubles with database interaction.
     * @throws ProcessingException    if request body contains not valid data.
     * @throws IOException            if had some troubles with getting reader from the request.
     */
    public String createMessageFromHttpRequest(EntityType type, CustomHttpServletRequestWrapper req) throws ClassNotFoundException, SQLException, ProcessingException, IOException {
        String url = req.getRequestURL().toString();
        String[] splittedUrl = url.split("/");
        String name = splittedUrl[NAME_INDEX_IN_URL];
        StringBuilder sb = new StringBuilder(type.toString()).append(";");
        sb.append(name).append(";");
        sb.append(req.getMethod()).append(";");
        if (req.getMethod().equals("GET") || req.getMethod().equals("DELETE")) {
            String limit = req.getParameter("limit");
            String page = req.getParameter("page");
            if (limit != null && page != null) {
                sb.append(limit).append(";").append(page);
            } else {
                if (type.equals(EntityType.OBJECT)) {
                    sb.append(splittedUrl[ID_INDEX_IN_URL]);
                }
            }
        } else if (req.getMethod().equals("PUT")) {
            if (splittedUrl[4].equals("Collections"))
                sb.append(req.getReader().lines().collect(Collectors.joining()));
            else {
                sb.append(servletUtils.createEntityFromRequest(req).toString());
            }
        } else {
            sb.append(servletUtils.createEntityFromRequest(req).toString());
        }
        return sb.toString();
    }
}
