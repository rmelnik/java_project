package by.home.taskX.JMS;

import by.home.taskX.Entities.Cat;
import by.home.taskX.Entities.DBEntity;
import by.home.taskX.Utils.NodeList;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.jms.*;
import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * Sends the message from constructor to the remote node.
 */
public class Client implements MessageListener {
    private static final Logger LOGGER = LogManager.getLogger(Client.class);
    private static final int ackMode;
    private static final String clientQueueName;
    private List<DBEntity> list;
    private Session session;
    private boolean isStaged = false;
    private boolean isCommited = false;
    private boolean transacted = false;
    private Destination tempDest;
    private String port;

    static {
        clientQueueName = "client.messages";
        ackMode = Session.AUTO_ACKNOWLEDGE;
    }

    /**
     * This constructor used when you need to send message to the remote node and read answer from it.
     *
     * @param message message which will been send to the remote node.
     * @param list    list of entities which user required to read.
     * @param port    port of the remote node(used as primary identifier).
     * @throws JMSException if had some troubles with remote jms service broker interaction.
     */
    public Client(String message, List<DBEntity> list, String port) throws JMSException {
        this.list = list;
        this.port = port;
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        try {
            Connection connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(transacted, ackMode);
            Destination adminQueue = session.createQueue(clientQueueName + port);
            MessageProducer producer = session.createProducer(adminQueue);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            tempDest = session.createTemporaryQueue();
            MessageConsumer responseConsumer = session.createConsumer(tempDest);
            responseConsumer.setMessageListener(this);
            TextMessage txtMessage = session.createTextMessage();
            txtMessage.setText(message);
            txtMessage.setJMSReplyTo(tempDest);
            String correlationId = this.createRandomString();
            txtMessage.setJMSCorrelationID(correlationId);
            LOGGER.info(NodeList.getInstance().getPort() + ";Sending Message:" + message + "; To:" + port);
            producer.send(txtMessage);
        } catch (JMSException e) {
            LOGGER.warn("Troubles with getting the response from another node." + e);
            throw e;
        }
    }

    /**
     * This constructor used when you need to send message to several nodes transactionally.
     */
    public Client(Session session, String message, String port) throws JMSException {
        this.port = port;
        //ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        try {/*
            Connection connection = connectionFactory.createConnection();
            connection.start();*/
            this.session = session;
            sendMessage(session, message, port);
        } catch (JMSException e) {
            LOGGER.warn("Troubles with getting the response from another node." + e);
            throw e;
        }
    }

    /**
     * This method used for creating random CorrelationId for interaction between nodes.
     *
     * @return random string.
     */
    private String createRandomString() {
        Random random = new Random(System.currentTimeMillis());
        long randomLong = random.nextLong();
        return Long.toHexString(randomLong);
    }

    /**
     * Sends the transacted message, should be commited manually.
     *
     * @param session
     * @param message
     * @param port
     * @throws JMSException
     */
    public void sendMessage(Session session, String message, String port) throws JMSException {
        Destination adminQueue = session.createQueue(clientQueueName + port);
        MessageProducer producer = session.createProducer(adminQueue);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        Destination tempDest = session.createTemporaryQueue();
        MessageConsumer responseConsumer = session.createConsumer(tempDest);
        responseConsumer.setMessageListener(this);
        TextMessage txtMessage = session.createTextMessage();
        txtMessage.setText(message);
        txtMessage.setJMSReplyTo(tempDest);
        String correlationId = this.createRandomString();
        txtMessage.setJMSCorrelationID(correlationId);
        LOGGER.info(NodeList.getInstance().getPort() + ";Sending Message:" + message + "; To:" + port);
        producer.send(txtMessage);
    }

    public void sendCommitMessage() throws JMSException {
        LOGGER.info("Send the commit message to :" + port);
        this.sendMessage(this.session, "COMMIT", port);
    }

    public void sendRollbackMessage() throws JMSException {
        LOGGER.info("Send the rollback message to :" + port);
        this.sendMessage(this.session, "ROLLBACK", port);

    }

    /**
     * Handling the responses from the remote nodes.
     *
     * @param message
     */
    public void onMessage(Message message) {
        String messageText;
        try {
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                messageText = textMessage.getText();
                LOGGER.info("Response from " + port + ":" + messageText);
                if (messageText.equals(Server.messageState.COMMITTED.name())) {
                    isCommited = true;
                } else if (messageText.equals(Server.messageState.STAGED.name())) {
                    isStaged = true;
                } else if (!messageText.equals(Server.messageState.STAGED.name()) && list != null) {
                    if (messageText.length() > 0) {
                        for (String str : messageText.split("/")) {
                            ObjectMapper mapper = new ObjectMapper();
                            DBEntity entity = mapper.readValue(str, Cat.class);
                            list.add(entity);
                        }
                    }
                }
            }
        } catch (JMSException e) {
            LOGGER.warn("Troubles with getting text from the message." + e);
        } catch (IOException e) {
            LOGGER.warn("Troubles with parsing response into concrete objects." + e);
        }
    }

    public synchronized boolean isStaged() {
        return isStaged;
    }

    public synchronized boolean isCommited() {
        return isCommited;
    }
}