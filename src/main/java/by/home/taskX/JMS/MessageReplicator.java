package by.home.taskX.JMS;

import by.home.taskX.Entities.NodeConfiguration;
import by.home.taskX.Utils.CustomHttpServletRequestWrapper;
import by.home.taskX.Utils.NodeList;
import by.home.taskX.Utils.ServletUtils;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageReplicator {
    private static MessageReplicator instance;
    private static final int ENTITY_TYPE_INDEX_IN_URL = 4;
    private static final int ID_INDEX_IN_URL = 6;

    public synchronized void replicateMessageToAnotherNodes(List<NodeConfiguration> replicas, String message) throws JMSException {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        Connection connection;
        connection = connectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(true,
                Session.SESSION_TRANSACTED);
        List<Client> clients = new ArrayList<>();
        for (NodeConfiguration replica : replicas) {
            clients.add(new Client(session, message,
                    replica.getPort()));
        }
        session.commit();
        checkIsStaged(session, clients, ServletUtils.TIMEOUT);
    }

    public static MessageReplicator getInstance() {
        if (instance == null) {
            instance = new MessageReplicator();
        }
        return instance;
    }

    /**
     * Shares the input data between replicas.
     *
     * @param req
     * @throws JMSException
     */
    public synchronized void replicateRequest(CustomHttpServletRequestWrapper req) throws JMSException, ClassNotFoundException, SQLException, IOException, ProcessingException {
        if (!req.getMethod().equals("GET")) {
            String[] splittedUrl = new ServletUtils().splitUrl(req);
            JMSMessageBuilder.EntityType type;
            if (splittedUrl[ENTITY_TYPE_INDEX_IN_URL].equals("Collections")) {
                type = JMSMessageBuilder.EntityType.COLLECTION;
            } else {
                type = JMSMessageBuilder.EntityType.OBJECT;
            }
            int index = splittedUrl[ID_INDEX_IN_URL].hashCode() % NodeList.getInstance().getOriginList().size();
            NodeConfiguration targetOrigin = NodeList.getInstance().getOriginList().get(index);
            List<NodeConfiguration> replicas = NodeList.getInstance().getGroupsMap().get(targetOrigin.getGroup());
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
            Connection connection = connectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(true, Session.SESSION_TRANSACTED);
            List<Client> clients = new ArrayList<>();
            for (NodeConfiguration replica : replicas) {
                clients.add(new Client(session, new JMSMessageBuilder().createMessageFromHttpRequest(type, req),
                        replica.getPort()));
            }
            session.commit();
            //checkIsStaged(session, clients, ServletUtils.TIMEOUT);
            /*for (Client client : clients) {
                client.sendCommitMessage();
            }*/
            session.commit();
        }
    }

    /**
     * Throws an exception if couldn't get the responses from remote nodes in a timeout.
     *
     * @param clients list of Client objects which should get the responses from remote nodes.
     * @param timeout time for getting all the responses in millis.
     * @throws JMSException
     */
    public void checkIsStaged(Session session, List<Client> clients, int timeout) throws JMSException {
        Map<Integer, Boolean> deliveryMap = new HashMap<>();
        boolean isTheResponsesGotten = false;
        long startTime = System.currentTimeMillis();
        setMap(deliveryMap, clients);
        while (!isTheResponsesGotten) {
            if (deliveryMap.values().contains(false)) {
                setMap(deliveryMap, clients);
            } else {
                isTheResponsesGotten = true;
            }
            if ((System.currentTimeMillis() - startTime) > timeout) {
                rollBack(session, deliveryMap, clients);
                throw new JMSException("Can't get any response before timeout.");
            }
        }
    }

    public void rollBack(Session session, Map<Integer, Boolean> deliveryMap, List<Client> clients) throws JMSException {
        for (Map.Entry entry : deliveryMap.entrySet()) {
            if ((Boolean) entry.getValue()) {
                clients.get((Integer) entry.getKey()).sendRollbackMessage();
            }
        }
        Math.sin(1);
        session.commit();
    }

    private void setMap(Map<Integer, Boolean> deliveryMap, List<Client> clients) {
        for (int i = 0; i < clients.size(); i++) {
            deliveryMap.put(i, clients.get(i).isStaged());
        }
    }
}
