package by.home.taskX.Utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private static ConnectionPool instance;
    private static final int POOL_CAPACITY = 100;

    private final Queue<Connection> availableConnections = new ArrayBlockingQueue<>(POOL_CAPACITY);
    private final Queue<Connection> usedConnections = new ArrayBlockingQueue<>(POOL_CAPACITY);

    private ConnectionPool() throws IOException, SQLException {
        for (int i = 0; i < POOL_CAPACITY; i++) {
            availableConnections.add(getDataSource().getConnection());
        }
    }

    public synchronized Connection retrieve() throws SQLException, IOException {
        Connection newConn = null;
        if (availableConnections.size() == 0) {
            newConn = getDataSource().getConnection();
        } else {
            newConn = availableConnections.poll();
        }
        usedConnections.add(newConn);
        return newConn;
    }

    public synchronized void putback(Connection c) throws IllegalArgumentException {
        if (c != null && usedConnections.poll() != null) {
            availableConnections.add(c);
        } else {
            throw new IllegalArgumentException("Connection not from the connection pool.");
        }
    }

    public static ConnectionPool getInstance() throws IOException, SQLException {
        synchronized (ConnectionPool.class) {
            if (instance == null) {
                instance = new ConnectionPool();
            }
        }
        return instance;
    }

    private DataSource getDataSource() throws IOException {
        JdbcDataSource dataSource = new JdbcDataSource();
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(getClass().getClassLoader().getResource("DBCredentials.json").getFile());
        List<String> strs = java.nio.file.Files.readAllLines(file.toPath(), Charsets.UTF_8);
        String str = strs.stream().collect(Collectors.joining());
        JsonNode node = mapper.readTree(str);
        dataSource.setURL(node.get("url").toString().replace("*", NodeList.getInstance().getPort()).replaceAll("[\"]", ""));
        dataSource.setUser(node.get("user").toString().replaceAll("[\"]", ""));
        dataSource.setPassword(node.get("password").toString().replaceAll("[\"]", ""));
        return dataSource;
    }

    public void closeAllConnections() {
        List<Connection> connections = new ArrayList<>(availableConnections);
        connections.addAll(usedConnections);
        try {
            for (Connection c : connections) {
                c.close();
            }
        } catch (SQLException e) {
            LOGGER.warn("Can't close the connection." + e);
        }
    }
}
