package by.home.taskX.Utils;

import by.home.taskX.DAO.DAOFactory;
import by.home.taskX.DAO.ObjectDAOImpl;
import by.home.taskX.Entities.DBEntity;
import by.home.taskX.Entities.NodeConfiguration;
import by.home.taskX.JMS.JMSMessageBuilder;
import by.home.taskX.JMS.MessageReplicator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import javax.jms.JMSException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public class Rebalancer {
    private static final Logger LOGGER = LogManager.getLogger(Rebalancer.class);
    private boolean isRebalanced = false;

    public void rebalance() {
        LOGGER.info("Rebalance started.");
        if (!isRebalanced) {
            try {
                Set<Class<? extends DBEntity>> set = new Reflections("by.training.taskX.Entities").getSubTypesOf(DBEntity.class);
                for (Class<? extends DBEntity> clazz : set) {
                    try {
                        ObjectDAOImpl objectDAO = (ObjectDAOImpl) new DAOFactory().getDAO(DAOFactory.DAOType.OBJECT, clazz);
                        int recordsCount = objectDAO.getRecordsCount();
                        if (recordsCount > 0) {
                            for (int i = 1; i <= recordsCount; i++) {
                                DBEntity entity = objectDAO.read(i);
                                List<NodeConfiguration> originsList = NodeList.getInstance().getOriginList();
                                int groupIndex = entity.getId().hashCode() % originsList.size();
                                String currentGroup = NodeList.getInstance().getCurrentNode().getGroup();
                                if (!originsList.get(groupIndex).getGroup().equals(currentGroup)) {
                                    List<NodeConfiguration> list = NodeList.getInstance().getGroupsMap().get(originsList.get(groupIndex).getGroup());
                                    String message = new JMSMessageBuilder().createMessage(
                                            JMSMessageBuilder.EntityType.OBJECT,
                                            clazz.getSimpleName(),
                                            JMSMessageBuilder.MethodType.PUT,
                                            new ObjectMapper().writeValueAsString(entity));
                                    MessageReplicator.getInstance().replicateMessageToAnotherNodes(list, message);
                                    objectDAO.delete(entity.getId());
                                    i--;
                                    recordsCount--;
                                }
                            }
                        }
                    } catch (JMSException e) {
                        LOGGER.warn("Error during JMS interaction." + e);
                    }
                }
            } catch (SQLException e) {
                LOGGER.warn("Rebalancer caught the SQLException. " + e);
            } catch (IllegalAccessException e) {
                LOGGER.warn("Rebalancer caught the IllegalAccessException." + e);
            } catch (InstantiationException e) {
                LOGGER.warn("Rebalancer caught the InstantiationException." + e);
            } catch (IOException e) {
                LOGGER.warn("Rebalancer caught the IOException." + e);
            }
            isRebalanced = true;
            LOGGER.info("Rebalance finished.");
        } else {
            throw new IllegalStateException("Node is already rebalanced");
        }
    }

}
