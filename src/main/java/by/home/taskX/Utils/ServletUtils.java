package by.home.taskX.Utils;

import by.home.taskX.Caches.Cache;
import by.home.taskX.Caches.CacheFactory;
import by.home.taskX.DAO.CollectionsDAOImpl;
import by.home.taskX.Entities.CollectionsEntity;
import by.home.taskX.Entities.DBEntity;
import by.home.taskX.Entities.NodeConfiguration;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class ServletUtils {
    private static final Logger LOGGER = LogManager.getLogger(ServletUtils.class);
    private static final String PREFIX = "by.training.taskX.Entities.";
    private static final int ENTITY_TYPE_INDEX_IN_URL = 4;
    private static final int CLASS_NAME_INDEX_IN_URL = 5;
    private static final int ID_INDEX_IN_URL = 6;
    public static final int TIMEOUT = 10000;
    private CollectionsDAOImpl collectionsDAO;
    private ProcessingReport report;
    private final ObjectMapper mapper = new ObjectMapper();

    public DBEntity createEntityFromRequest(CustomHttpServletRequestWrapper req) throws IOException, SQLException, ProcessingException, ClassNotFoundException {
        String[] splittedUrl = splitUrl(req);
        String name = splittedUrl[CLASS_NAME_INDEX_IN_URL];
        Class clazz = Class.forName(PREFIX + name);
        String jsonString = req.getBody();
        collectionsDAO = new CollectionsDAOImpl();
        validate(jsonString, collectionsDAO.read(name).getJsonSchema());
        if (report.isSuccess()) {
            DBEntity entity = (DBEntity) mapper.readValue(jsonString, clazz);
            if (splittedUrl.length > ID_INDEX_IN_URL) {
                entity.setId(splittedUrl[ID_INDEX_IN_URL]);
            } else {
                entity.setId(UUID.randomUUID().toString());
            }
            return entity;
        } else {
            throw new IllegalArgumentException("Wrong JSON object.");
        }
    }

    public CollectionsEntity createCollectionFromRequest(HttpServletRequest req) throws IOException {
        String jsonString = req.getReader().lines().collect(Collectors.joining());
        return mapper.readValue(jsonString, CollectionsEntity.class);
    }

    /**
     * validates whether the inputed json string satisfy the json schema.
     *
     * @param json
     * @param schema
     * @throws IOException
     * @throws ProcessingException
     */
    private void validate(String json, String schema)
            throws IOException, ProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(json);
        JsonSchema jsonSchema = JsonSchemaFactory.byDefault().getJsonSchema(mapper.readTree(schema));
        this.report = jsonSchema.validate(jsonNode);
    }

    /**
     * Checks whether the inputed request should be redirected to another node.
     *
     * @param req inputed request.
     * @return true if should to be redirected, false if not.
     */
    public boolean isRequestNeedsToBeRedirected(CustomHttpServletRequestWrapper req) throws IOException, SQLException, ProcessingException, ClassNotFoundException {
        List<NodeConfiguration> nodes = NodeList.getInstance().getOriginList();
        String[] splittedUrl = req.getRequestURL().toString().split("/");
        String id;
        if (splittedUrl.length > ID_INDEX_IN_URL) {
            id = splittedUrl[ID_INDEX_IN_URL];
        } else {
            id = createEntityFromRequest(req).getId();
        }
        int i = id.hashCode() % nodes.size();
        return !nodes.get(i).getGroup().equals(NodeList.getInstance().getCurrentNode().getGroup());
    }


    /**
     * Returns the cache which may contain the objects inherited from DBEntity class,
     * with properties which correspond the same named collection in DB.
     *
     * @return the filled cache
     */
    public Cache createCache(Class<? extends DBEntity> clazz) throws SQLException, IOException {
        Cache cache;
        collectionsDAO = new CollectionsDAOImpl();
        CollectionsEntity collectionsEntity = collectionsDAO.read(clazz.getSimpleName());
        cache = new CacheFactory().getCache(collectionsEntity.getType(), collectionsEntity.getMaxCount());
        return cache;
    }

    /**
     * Creates the map of created caches, where key is the name of correspond entity.
     *
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public Map<String, Cache> createCacheMap() throws IOException, SQLException {
        Set<Class<? extends DBEntity>> set = new Reflections("by.training.taskX.Entities").getSubTypesOf(DBEntity.class);
        Map<String, Cache> cacheMap = new HashMap<>();
        for (Class clazz : set) {
            cacheMap.put(clazz.getSimpleName(), createCache(clazz));
        }
        return cacheMap;
    }

    /**
     * Splits request url and returns the array of segments.
     *
     * @param req
     * @return array of the url segments.
     */
    public String[] splitUrl(HttpServletRequest req) {
        String url = req.getRequestURL().toString();
        return url.split("/");
    }


    /**
     * Checks wheter the specified entity is gotten from another group.
     *
     * @param entity
     * @return
     */
    public boolean isAlienEntity(DBEntity entity) {
        List<NodeConfiguration> listOfOrigins = NodeList.getInstance().getOriginList();
        int index = entity.getId().hashCode() % listOfOrigins.size();
        NodeConfiguration currentNode = NodeList.getInstance().getCurrentNode();
        return listOfOrigins.get(index).getGroup().equals(currentNode.getGroup());
    }
}
