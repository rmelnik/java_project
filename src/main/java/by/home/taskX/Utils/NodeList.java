package by.home.taskX.Utils;

import by.home.taskX.Entities.NodeConfiguration;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.base.Charsets;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class NodeList {
    private static final Logger LOGGER = LogManager.getLogger(NodeList.class);
    private final List<NodeConfiguration> list;
    private static NodeList instance;

    /**
     * Reads the main configuration file, puts it's content into the list.
     */
    private NodeList() {
        list = new ArrayList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            URL url = getClass().getClassLoader().getResource("Configuration.json");
            File file;
            if (url != null) {
                file = new File(url.getFile());
                List<String> strs = java.nio.file.Files.readAllLines(file.toPath(), Charsets.UTF_8);
                String str = strs.stream().collect(Collectors.joining());
                ArrayNode arrayNode = (ArrayNode) mapper.readTree(str);
                for (JsonNode node : arrayNode) {
                    list.add(mapper.readValue(node.toString(), NodeConfiguration.class));
                }
            }
        } catch (IOException e) {
            LOGGER.warn("Troubles while reads the configuration of the cluster. System is shutting down.");
            System.exit(-1);
        }
    }

    public static NodeList getInstance() {
        if (instance == null) {
            instance = new NodeList();
        }
        return instance;
    }

    /**
     * @return the map of lists divided by groups.
     */
    public Map<String, List<NodeConfiguration>> getGroupsMap() {
        Map<String, List<NodeConfiguration>> groups = new HashMap<>();
        for (NodeConfiguration node : list) {
            List<NodeConfiguration> mapList = groups.get(node.getGroup());
            if (mapList == null) {
                mapList = new ArrayList<>();
            }
            mapList.add(node);
            groups.put(node.getGroup(), mapList);
        }
        return groups;
    }

    /**
     * @return list of nodes which placed in the same group as current one.
     */
    public List<NodeConfiguration> getListOfReplicas() {
        List<NodeConfiguration> list = NodeList.getInstance().getList();
        List<NodeConfiguration> listOfReplicas = null;
        String curPort = getPort();
        for (NodeConfiguration node : list) {
            if (node.getPort().equals(curPort)) {
                listOfReplicas = new ArrayList<>(NodeList.getInstance().getGroupsMap().get(node.getGroup()));
                for (int i = 0; i < listOfReplicas.size(); i++) {
                    if (listOfReplicas.get(i).getPort().equals(curPort)) {
                        listOfReplicas.remove(i);
                        i--;
                    }
                }
            }
        }
        return listOfReplicas;
    }

    /**
     * @return list of nodes which are not marked as replicas.
     */
    public List<NodeConfiguration> getOriginList() {
        List<NodeConfiguration> originList = new ArrayList<>();
        for (NodeConfiguration node : list) {
            if (!node.getIsReplica()) {
                originList.add(node);
            }
        }
        return originList;
    }

    /**
     * @return NodeConfiguration object which represents the current node.
     */
    public NodeConfiguration getCurrentNode() {
        for (NodeConfiguration node : list) {
            if (node.getPort().equals(getPort())) {
                return node;
            }
        }
        return null;
    }

    /**
     * @return port of current node.
     */
    public String getPort() {
        MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();
        Set<ObjectName> objectNames;
        String port = null;
        try {
            objectNames = beanServer.queryNames(new ObjectName("*:type=Connector,*"),
                    Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
            port = objectNames.iterator().next().getKeyProperty("port");
        } catch (MalformedObjectNameException e) {
            LOGGER.warn("Troubles while get's the current node port. System is shutting down.");
            System.exit(-1);
        }
        return port;
    }

    public List<NodeConfiguration> getList() {
        return new ArrayList<>(list);
    }
}
