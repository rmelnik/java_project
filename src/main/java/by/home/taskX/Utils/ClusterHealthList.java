package by.home.taskX.Utils;

import by.home.taskX.Entities.NodeConfiguration;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Checks the cluster working state. Pings all nodes specified in configuration file.
 */
public class ClusterHealthList {
    private static final Logger LOGGER = LogManager.getLogger(ClusterHealthList.class);
    private final static Map<String, Boolean> nodeMap = new ConcurrentHashMap<>();
    private static volatile ClusterHealthList instance;
    private static volatile boolean isWorking;
    private static boolean isRebalanced=false;
    private static final int TIMEOUT = 1500;

    private ClusterHealthList() {
        List<NodeConfiguration> list = NodeList.getInstance().getList();
        String nodeName;
        for (int i = 0; i < list.size(); i++) {
            nodeName = list.get(i).getHost() + ":" + list.get(i).getPort();
            if (list.get(i).getPort().equals(NodeList.getInstance().getPort())) {
                list.remove(i);
                i--;
            } else {
                nodeMap.put(nodeName, false);
                new Thread(new CheckHealthThread(nodeName)).start();
            }
        }
        new Thread(() -> {
            synchronized (this) {
                while (isWorking) {
                    try {
                        checkClusterState();
                        wait(TIMEOUT);
                    } catch (InterruptedException e) {
                        LOGGER.warn("Health check was interrupted." + e);
                    }
                }
            }
        }).start();
    }

    /**
     * This thread check the health of specified node in background.
     */
    private class CheckHealthThread implements Runnable {
        private final String node;

        CheckHealthThread(String node) {
            this.node = node;
        }

        @Override
        public void run() {
            synchronized (this) {
                while (isWorking) {
                    checkNodeState(node);
                    try {
                        wait(TIMEOUT);
                    } catch (InterruptedException e) {
                        LOGGER.warn("Health check of the '" + node + "' was interrupted." + e);
                    }
                }
            }
        }
    }

    /**
     * Sends the GET request to the targeted node and check whether it's working.
     *
     * @param url url of the targeted node.
     */
    private void checkNodeState(String url) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet get = new HttpGet(url + "/health");
        try {
            HttpResponse response = httpclient.execute(get);
            if (response.getStatusLine().getStatusCode() == 200) {
                nodeMap.put(url, true);
            } else {
                nodeMap.put(url, false);
            }
        } catch (IOException e) {
            nodeMap.put(url, false);
            LOGGER.warn(url + " Don't response.");
        }
    }

    /**
     * Check whether all nodes in claster is working.
     *
     * @return true if working, false if at least one of them not working.
     */
    public boolean checkClusterState() {
        return nodeMap.values().stream().allMatch(state -> state);
    }

    public static ClusterHealthList getInstance() {
        synchronized (ClusterHealthList.class) {
            if (instance == null) {
                instance = new ClusterHealthList();
            }
        }
        return instance;
    }

    public static void setIsWorking(boolean isWorking) {
        ClusterHealthList.isWorking = isWorking;
    }
}
