package by.home.taskX.Servlets;

import by.home.taskX.Caches.Cache;
import by.home.taskX.Entities.DBEntity;
import by.home.taskX.JMS.MessageReplicator;
import by.home.taskX.Services.ObjectService;
import by.home.taskX.Utils.CustomHttpServletRequestWrapper;
import by.home.taskX.Utils.ServletUtils;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;*/
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class ObjectServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(ObjectServlet.class);
    private ObjectService objectService;
    private boolean isCacheFilled;
    private ServletUtils servletUtils;
    private static final int CLASS_NAME_INDEX_IN_URL = 5;
    private static final int ID_INDEX_IN_URL = 6;
    private Map<String, Cache> cacheMap;

    @Override
    public void init() throws ServletException {
        isCacheFilled = false;
        objectService = new ObjectService();
        servletUtils = new ServletUtils();
        try {
            cacheMap = servletUtils.createCacheMap();
        } catch (IOException e) {
            LOGGER.warn("Error while read some collection.Check whether each DBEntity descendant have the correspond collection in DB." + e.toString());
        } catch (SQLException e) {
            LOGGER.warn("Troubles with db interaction." + e.toString());
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] splittedUrl = servletUtils.splitUrl(req);
        String clazz = splittedUrl[CLASS_NAME_INDEX_IN_URL];
        try {
            String limit = req.getParameter("limit");
            String page = req.getParameter("page");
            if (limit != null && page != null) {
                List<DBEntity> list = objectService.getAllObjects(clazz, Integer.parseInt(limit), Integer.parseInt(page));
                resp.getWriter().print(list);
            } else {
                String id = splittedUrl[ID_INDEX_IN_URL];
                if (cacheMap.get(clazz).getEntity(id) != null) {
                    resp.getWriter().print(cacheMap.get(clazz).getEntity(id));
                } else {
                    DBEntity entity = objectService.getObjectById(id, clazz);
                    if (!servletUtils.isAlienEntity(entity)) {
                        cacheMap.get(clazz).addEntity(entity);
                    }
                    resp.getWriter().print(entity);
                }
            }
        } catch (ClassNotFoundException e) {
            resp.setStatus(404);
            String message = "Specified entity was not registered in the system." + e.toString();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (SQLException e) {
            resp.setStatus(521);
            String message = "Troubles with getting the database interaction." + e.toString();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (IllegalAccessException e) {
            resp.setStatus(522);
            String message = "Can't get access to the requested element." + e.toString();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (InstantiationException e) {
            resp.setStatus(523);
            String message = "Can't instantiate internal object." + e.toString();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (IOException e) {
            resp.setStatus(524);
            String message = "Troubles with processing response." + e.toString();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (JMSException e) {
            resp.setStatus(525);
            String message = "Troubles with getting response from another node." + e.toString();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            CustomHttpServletRequestWrapper wrapper = new CustomHttpServletRequestWrapper(req);
            if (servletUtils.isRequestNeedsToBeRedirected(wrapper)) {
                MessageReplicator.getInstance().replicateRequest(wrapper);
            } else {
                DBEntity entity = servletUtils.createEntityFromRequest(wrapper);
                objectService.insertObject(entity);
                MessageReplicator.getInstance().replicateRequest(wrapper);
            }
        } catch (ProcessingException e) {
            resp.setStatus(400);
            String message = "Wrong object was requested to add." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (ClassNotFoundException e) {
            resp.setStatus(404);
            String message = "Specified entity was not registered in the system." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (SQLException e) {
            resp.setStatus(521);
            String message = "Troubles with getting the database interaction." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (IOException e) {
            resp.setStatus(524);
            String message = "Troubles with processing response." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (JMSException e) {
            resp.setStatus(525);
            String message = "Troubles with getting response from another node." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        }
    }

    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] splittedUrl = servletUtils.splitUrl(req);
        String id = splittedUrl[ID_INDEX_IN_URL];
        String clazz = splittedUrl[CLASS_NAME_INDEX_IN_URL];
        try {
            CustomHttpServletRequestWrapper wrapper = new CustomHttpServletRequestWrapper(req);
            if (servletUtils.isRequestNeedsToBeRedirected(wrapper)) {
                MessageReplicator.getInstance().replicateRequest(wrapper);
            } else {
                DBEntity entity = servletUtils.createEntityFromRequest(wrapper);
                objectService.updateObject(entity);
                MessageReplicator.getInstance().replicateRequest(wrapper);
            }
            if (isCacheFilled && cacheMap.get(clazz).getMap().get(id) != null) {
                cacheMap.get(clazz).getMap().remove(id);
            }
        } catch (ProcessingException e) {
            resp.setStatus(400);
            String message = "Wrong object was requested to add." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (ClassNotFoundException e) {
            resp.setStatus(404);
            String message = "Specified entity was not registered in the system." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (SQLException e) {
            resp.setStatus(521);
            String message = "Troubles with getting the database interaction." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (IOException e) {
            resp.setStatus(524);
            String message = "Troubles with processing response." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (JMSException e) {
            resp.setStatus(525);
            String message = "Troubles with getting response from another node." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] splittedUrl = servletUtils.splitUrl(req);
        String id = splittedUrl[ID_INDEX_IN_URL];
        String clazz = splittedUrl[CLASS_NAME_INDEX_IN_URL];
        try {
            CustomHttpServletRequestWrapper wrapper = new CustomHttpServletRequestWrapper(req);
            if (servletUtils.isRequestNeedsToBeRedirected(wrapper)) {
                MessageReplicator.getInstance().replicateRequest(wrapper);
            } else {
                MessageReplicator.getInstance().replicateRequest(wrapper);
                objectService.deleteObject(servletUtils.splitUrl(req)[CLASS_NAME_INDEX_IN_URL], id);
            }
            if (isCacheFilled && cacheMap.get(clazz).getMap().get(id) != null) {
                cacheMap.get(clazz).getMap().remove(id);
            }
        } catch (ProcessingException e) {
            resp.setStatus(400);
            String message = "Wrong object was requested to add." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (ClassNotFoundException e) {
            resp.setStatus(404);
            String message = "Specified entity was not registered in the system." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (SQLException e) {
            resp.setStatus(521);
            String message = "Troubles with getting the database interaction." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (IOException e) {
            resp.setStatus(524);
            String message = "Troubles with processing response." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (JMSException e) {
            resp.setStatus(525);
            String message = "Troubles with getting response from another node." + e;
            LOGGER.warn(message);
            resp.getWriter().print(message);
        }
    }
}
