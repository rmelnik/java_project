package by.home.taskX.Servlets;

import by.home.taskX.JMS.JMSMessageBuilder;
import by.home.taskX.Services.CollectionsService;
import by.home.taskX.Entities.CollectionsEntity;
import by.home.taskX.JMS.MessageReplicator;
import by.home.taskX.Entities.NodeConfiguration;
import by.home.taskX.Utils.CustomHttpServletRequestWrapper;
import by.home.taskX.Utils.NodeList;
import by.home.taskX.Utils.ServletUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.List;

public class CollectionsServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(CollectionsServlet.class);
    private CollectionsService service;
    private ServletUtils servletUtils;
    private static final int CLASS_NAME_INDEX_IN_URL = 4;
    private static final int COLLECTION_NAME_INDEX_IN_URL = 5;

    /**
     * Initializes some fields.
     *
     * @throws ServletException common exception for initialising error.
     */
    @Override
    public void init() throws ServletException {
        try {
            service = new CollectionsService();
            servletUtils = new ServletUtils();
        } catch (SQLException e) {
            LOGGER.warn("Servlet initialisation ends with SQLException. Troubles with DB interaction." + e.getMessage());
            throw new ServletException(e);
        } catch (IOException e) {
            LOGGER.warn("Servlet initialisation ends with IOException. Probably troubles with connection pool." + e.getMessage());
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        String[] splittedUrl = servletUtils.splitUrl(req);
        try {
            String limit = req.getParameter("limit");
            String page = req.getParameter("page");
            if (limit != null && page != null) {
                resp.getWriter().print(service.getCollections(Integer.parseInt(limit), Integer.parseInt(page)));
            } else if (splittedUrl.length >= CLASS_NAME_INDEX_IN_URL) {
                resp.getWriter().print(service.getCollection(splittedUrl[COLLECTION_NAME_INDEX_IN_URL]));
            } else {
                throw new MalformedURLException("Incorrect Url");
            }
        } catch (MalformedURLException e) {
            resp.setStatus(400);
            LOGGER.warn("Illegal URL structure.");
            throw new ServletException(e);
        } catch (IOException e) {
            resp.setStatus(524);
            LOGGER.warn("Can't get writer." + e);
            throw new ServletException(e);
        } catch (SQLException e) {
            resp.setStatus(521);
            LOGGER.warn("Troubles with database interaction." + e);
            throw new ServletException(e);
        }
    }


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            String[] splittedUrl = servletUtils.splitUrl(req);
            CollectionsEntity entity = servletUtils.createCollectionFromRequest(req);
            service.insertCollection(entity);
            String message = new JMSMessageBuilder().createMessage(
                    JMSMessageBuilder.EntityType.COLLECTION,
                    splittedUrl[COLLECTION_NAME_INDEX_IN_URL],
                    JMSMessageBuilder.MethodType.PUT,
                    new ObjectMapper().writeValueAsString(entity));
            List<NodeConfiguration> list = NodeList.getInstance().getList();
            list.remove(NodeList.getInstance().getCurrentNode());
            MessageReplicator.getInstance().replicateMessageToAnotherNodes(list
                    , message);
        } catch (IOException e) {
            resp.setStatus(524);
            LOGGER.warn("Can't get writer." + e);
            throw new ServletException(e);
        } catch (SQLException e) {
            resp.setStatus(521);
            LOGGER.warn("Troubles with database interaction." + e);
            throw new ServletException(e);
        } catch (JMSException e) {
            resp.setStatus(525);
            LOGGER.warn("Error during JMS interaction." + e.getMessage());
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String[] splittedUrl = servletUtils.splitUrl(req);
            CollectionsEntity entity = servletUtils.createCollectionFromRequest(req);
            service.updateCollection(entity);
            String message = new JMSMessageBuilder().createMessage(
                    JMSMessageBuilder.EntityType.COLLECTION,
                    splittedUrl[COLLECTION_NAME_INDEX_IN_URL],
                    JMSMessageBuilder.MethodType.POST,
                    new ObjectMapper().writeValueAsString(entity));
            List<NodeConfiguration> list = NodeList.getInstance().getList();
            list.remove(NodeList.getInstance().getCurrentNode());
            MessageReplicator.getInstance().replicateMessageToAnotherNodes(list
                    , message);
        } catch (IOException e) {
            resp.setStatus(524);
            LOGGER.warn("Can't get writer." + e.getMessage());
            String message = "Specified entity was not registered in the system." + e.toString();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (SQLException e) {
            resp.setStatus(521);
            String message = "Troubles with database interaction." + e.getMessage();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        } catch (JMSException e) {
            resp.setStatus(525);
            String message = "Error during JMS interaction." + e.getMessage();
            LOGGER.warn(message);
            resp.getWriter().print(message);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CustomHttpServletRequestWrapper wrapper = new CustomHttpServletRequestWrapper(req);
        try {
            String collectionName = servletUtils.splitUrl(req)[COLLECTION_NAME_INDEX_IN_URL];
            service.deleteCollection(collectionName);
            MessageReplicator.getInstance().replicateRequest(wrapper);
        } catch (SQLException e) {
            resp.sendError(521);
            LOGGER.warn("Troubles with database interaction." + e.getMessage());
            throw new ServletException(e);
        } catch (ClassNotFoundException e) {
            resp.setStatus(404);
            LOGGER.warn("Illegal URL structure." + e.getMessage());
            throw new ServletException(e);
        } catch (ProcessingException e) {
            resp.setStatus(400);
            LOGGER.warn("Error while writing the message for another node." + e.getMessage());
            throw new ServletException(e);
        } catch (JMSException e) {
            resp.setStatus(525);
            LOGGER.warn("Troubles with sending message to another node." + e.getMessage());
            throw new ServletException(e);
        }
    }
}
