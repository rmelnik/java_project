package by.home.taskX.Entities;

/**
 * Common parent for all entities which should interact with the ObjectDAOImpl class.
 */
public abstract class DBEntity {
    private String id;
    private String value;

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DBEntity entity = (DBEntity) o;

        return (id != null ? id.equals(entity.id) : entity.id == null) && (value != null ? value.equals(entity.value) : entity.value == null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + '\"' +
                ", \"value\":\"" + value + '\"' +
                '}';
    }
}
