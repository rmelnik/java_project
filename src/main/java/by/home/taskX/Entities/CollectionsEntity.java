package by.home.taskX.Entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Entity which should interact with ObjectDAOImpl class.
 */
public class CollectionsEntity {
    private String name;
    private String type;
    private String jsonSchema;
    private int maxCount;

    public CollectionsEntity() {
    }

    public CollectionsEntity(String type, int maxCount, String jsonSchema) {
        this.type = type;
        this.maxCount = maxCount;
        this.jsonSchema = jsonSchema;
    }

    public CollectionsEntity(String name, String type, String jsonSchema, int maxCount) {
        this.name = name;
        this.type = type;
        this.jsonSchema = jsonSchema;
        this.maxCount = maxCount;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJsonSchema() {
        return jsonSchema;
    }

    public void setJsonSchema(String jsonSchema) {
        this.jsonSchema = jsonSchema;
    }

    @Override
    public String toString() {
        String result = null;
        try {
            result = new ObjectMapper().writeValueAsString(this);/*"{" +
                    "\"name\":\"" + name + "\"" +
                    ",\"type\":\"" + type + "\"" +
                    ",\"jsonSchema\":\"" + jsonSchema.replaceAll("\"","\"") + "\"" +
                    ",\"maxCount\":\"" + maxCount +
                    '}';*/
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}
