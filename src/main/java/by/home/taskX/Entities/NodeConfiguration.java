package by.home.taskX.Entities;

/**
 * This Pojo represents the properties of nodes in current cluster and provides access to it.
 */
public class NodeConfiguration {
    private String host;
    private String port;
    private String group;
    private Boolean isReplica;

    public NodeConfiguration() {
    }

    public NodeConfiguration(String host, String port, String group, Boolean isReplica) {
        this.host = host;
        this.port = port;
        this.group = group;
        this.isReplica = isReplica;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getGroup() {
        return group;
    }

    public Boolean getIsReplica() {
        return isReplica;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeConfiguration that = (NodeConfiguration) o;

        return host.equals(that.host) && port.equals(that.port) && group.equals(that.group) && isReplica.equals(that.isReplica);
    }

    @Override
    public int hashCode() {
        return 31 * host.hashCode() + port.hashCode() + group.hashCode() + isReplica.hashCode();
    }
}
