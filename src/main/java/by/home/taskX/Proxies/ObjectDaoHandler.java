package by.home.taskX.Proxies;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import by.home.taskX.DAO.ObjectDAO;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Dynamic proxy which do logging of the working time for ObjectDao class methods, which marked by @AclLog annotation.
 */
public class ObjectDaoHandler implements InvocationHandler {
    private static final Logger LOGGER = LogManager.getLogger(ObjectDaoHandler.class);
    private final ObjectDAO objectDao;

    private ObjectDaoHandler(ObjectDAO objectDao) {
        this.objectDao = objectDao;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object meth;
        int argsCount = method.getParameterCount();
        long start = System.nanoTime();
        Object[] curArgs = new Object[argsCount];
        StringBuilder arguments = new StringBuilder();
        for (int i = 0; i < argsCount; i++) {
            curArgs[i] = args[i];
            arguments.append(args[i]).append(";");
        }
        meth = method.invoke(objectDao, curArgs);
        long end = System.nanoTime();
        if (method.getAnnotation(AclLog.class) != null) {
            LOGGER.info(method.getName() + ". With arg/s:" + arguments + ". Ends with:" + (end - start));
        }
        return meth;
    }
}
