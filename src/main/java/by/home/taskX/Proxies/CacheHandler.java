package by.home.taskX.Proxies;

import by.home.taskX.Caches.Cache;
import by.home.taskX.Entities.DBEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/*import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;*/

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CacheHandler implements InvocationHandler {

    private static final Logger LOGGER = LogManager.getLogger(CacheHandler.class);
    private final Cache cache;

    public CacheHandler(Cache cache) {
        this.cache = cache;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long start = System.nanoTime();
        cache.addEntity((DBEntity) args[0]);
        long end = System.nanoTime();
        LOGGER.info("Cache put method takes:" + (end - start));
        return null;
    }
}
