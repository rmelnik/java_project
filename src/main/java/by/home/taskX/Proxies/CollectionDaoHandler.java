package by.home.taskX.Proxies;

import by.home.taskX.DAO.CollectionDAO;/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
*/
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Dynamic proxy which do logging of the working time for CollectionDaoImpl class methods, which marked by @AclLog annotation.
 */
public class CollectionDaoHandler implements InvocationHandler {
    private static final Logger LOGGER = LogManager.getLogger(CollectionDaoHandler.class);
    private final CollectionDAO dao;

    public CollectionDaoHandler(CollectionDAO dao) {
        this.dao = dao;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long start = System.nanoTime();
        int argsCount = method.getParameterCount();
        Object[] curArgs = new Object[argsCount];
        StringBuilder arguments = new StringBuilder();
        for (int i = 0; i < argsCount; i++) {
            curArgs[i] = args[i];
            arguments.append(args[i]).append(";");
        }
        Object meth;
        meth = method.invoke(dao, curArgs);
        long end = System.nanoTime();
        if (method.getAnnotation(AclLog.class) != null) {
            LOGGER.info(method.getName() + ". With arg/s:" + arguments.toString() + ". Ends with:" + (end - start));
        }
        return meth;
    }
}
