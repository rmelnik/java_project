package by.home.taskX.Services;

import by.home.taskX.DAO.CollectionsDAOImpl;
import by.home.taskX.Entities.CollectionsEntity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class CollectionsService {
    private CollectionsDAOImpl collectionsDAO;

    /**
     * Simply initializes the collectionDAO object;
     *
     * @throws SQLException if had some troubles with DB interaction.
     * @throws IOException  if had some troubles with connection pool.
     */
    public CollectionsService() throws SQLException, IOException {
        collectionsDAO = new CollectionsDAOImpl();
    }

    /**
     * Returns the collection entity with the name specified in URL.
     *
     * @return
     * @throws SQLException if had some troubles with DB interaction.
     * @throws IOException  if had some troubles with connection pool.
     */
    public CollectionsEntity getCollection(String name) throws IOException, SQLException {
        return collectionsDAO.read(name);
    }

    /**
     * Returns the list of collection entities which name.
     *
     * @return the list of entities which satisfy the conditions.
     * @throws SQLException if had some troubles with DB interaction.
     * @throws IOException  if had some troubles with connection pool.
     */
    public List<CollectionsEntity> getCollections(int limit, int page) throws IOException, SQLException {
        return collectionsDAO.read(limit, page);
    }

    /**
     * Insert collection in DB.
     *
     * @throws SQLException if had some troubles with DB interaction.
     * @throws IOException  if had some troubles with connection pool.
     */
    public void insertCollection(CollectionsEntity entity) throws IOException, SQLException {
        collectionsDAO.create(entity);
    }

    /**
     * Update collection in DB.
     *
     * @throws SQLException if had some troubles with DB interaction.
     * @throws IOException  if had some troubles with connection pool.
     */
    public void updateCollection(CollectionsEntity entity) throws IOException, SQLException {
        collectionsDAO.update(entity, entity.getName());
    }

    /**
     * Delete collection from DB by the collection name.
     *
     * @throws SQLException if had some troubles with DB interaction.
     * @throws IOException  if had some troubles with connection pool.
     */
    public void deleteCollection(String name) throws SQLException, IOException {
        collectionsDAO.delete(name);
    }
}
