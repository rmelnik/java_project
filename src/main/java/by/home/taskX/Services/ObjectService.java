package by.home.taskX.Services;

import by.home.taskX.DAO.DAOFactory;
import by.home.taskX.DAO.ObjectDAO;
import by.home.taskX.JMS.Client;
import by.home.taskX.JMS.JMSMessageBuilder;
import by.home.taskX.Utils.NodeList;
import by.home.taskX.Entities.DBEntity;
import by.home.taskX.Entities.NodeConfiguration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.jms.JMSException;
import java.io.*;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class ObjectService {

    private static final Logger LOGGER = LogManager.getLogger(ObjectService.class);
    private static final String PREFIX = "by.training.taskX.Entities.";
    private final DAOFactory daoFactory;
    private ObjectDAO objectDao;

    public ObjectService() {
        daoFactory = new DAOFactory();
    }

    /**
     * Return the object from db with specified id. If object is placed on remote node - sends the JMS message to get it.
     *
     * @param id    id of the requested object.
     * @param clazz type of the requested object.
     * @return DBEntity object with Specified id.
     * @throws ClassNotFoundException if trying to get the not existing entity.
     * @throws IOException            when had some troubles with DAO connection pool.
     * @throws SQLException           when had some troubles with DB interaction.
     * @throws IllegalAccessException when try to get access to unavailable resource.
     * @throws InstantiationException when had some troubles with creating the explicit entity.
     * @throws JMSException           when had troubles with JMS interaction.
     */
    public DBEntity getObjectById(String id, String clazz)
            throws ClassNotFoundException, IOException, SQLException, IllegalAccessException, InstantiationException, JMSException {
        List<NodeConfiguration> listOfOrigins = NodeList.getInstance().getOriginList();
        int index = id.hashCode() % listOfOrigins.size();
        DBEntity entity;
        NodeConfiguration targetNode = listOfOrigins.get(index);
        NodeConfiguration currentNode = NodeList.getInstance().getCurrentNode();
        if (targetNode.getGroup().equals(currentNode.getGroup())) {
            objectDao = (ObjectDAO) daoFactory.getDAO(DAOFactory.DAOType.OBJECT, Class.forName(PREFIX + clazz));
            entity = objectDao.read(id);
        } else {
            synchronized (this) {
                List<DBEntity> list = new CopyOnWriteArrayList<>();
                Client client = new Client(new JMSMessageBuilder().createMessage(
                        JMSMessageBuilder.EntityType.OBJECT,
                        clazz, JMSMessageBuilder.MethodType.GET,
                        id), list, targetNode.getPort());
                long startTime = System.currentTimeMillis();
                while (!client.isStaged()) {
                    if ((System.currentTimeMillis() - startTime) > 10000) {
                        throw new JMSException("Can't get any response before timeout.");
                    }
                }
                if (list.size() > 0) {
                    entity = list.get(0);
                } else {
                    throw new JMSException("No entity found by specified id:" + id);
                }
            }
        }
        return entity;
    }

    /**
     * Returns the list of object with specified class and positions.
     *
     * @param clazz type of the requested objects.
     * @param limit number of needed objects.
     * @param page  page.
     * @return the list of objects.
     * @throws ClassNotFoundException if trying to get the not existing entity.
     * @throws IOException            when had some troubles with DAO connection pool.
     * @throws SQLException           when had some troubles with DB interaction.
     * @throws IllegalAccessException when try to get access to unavailable resource.
     * @throws InstantiationException when had some troubles with creating the explicit entity.
     * @throws JMSException           when had troubles with JMS interaction.
     */
    public List<DBEntity> getAllObjects(String clazz, int limit, int page)
            throws ClassNotFoundException, IOException, SQLException, IllegalAccessException, InstantiationException, JMSException {
        objectDao = (ObjectDAO) daoFactory.getDAO(DAOFactory.DAOType.OBJECT, Class.forName(PREFIX + clazz));
        List<DBEntity> list = objectDao.read(limit, page);
        List<NodeConfiguration> listOfOrigins = NodeList.getInstance().getOriginList().stream().
                filter(node -> !node.getGroup().equals(NodeList.getInstance().getCurrentNode().getGroup())).collect(Collectors.toList());
        for (NodeConfiguration node : listOfOrigins) {
            synchronized (this) {
                try {
                    Client client = new Client(new JMSMessageBuilder().createMessage(
                            JMSMessageBuilder.EntityType.OBJECT,
                            clazz,
                            JMSMessageBuilder.MethodType.GET,
                            limit + ";" + page), list, node.getPort());
                    long startTime = System.currentTimeMillis();
                    while (!client.isStaged()) {
                        if ((System.currentTimeMillis() - startTime) > 10000) {
                            throw new JMSException("Can't get any response before timeout.");
                        }
                    }
                } catch (JMSException e) {
                    LOGGER.warn(e);
                    throw e;
                }
            }
        }
        list.sort(Comparator.comparing(DBEntity::getId));
        for(int i=limit;i<list.size();){
            list.remove(i);
        }
        return list;
    }

    /**
     * Adds the new entity to the DB.
     *
     * @param entity new entity.
     * @throws IOException  when had some troubles with DAO connection pool.
     * @throws SQLException when had some troubles with DB interaction.
     */
    public void insertObject(DBEntity entity)
            throws IOException, SQLException {
        objectDao = (ObjectDAO) daoFactory.getDAO(DAOFactory.DAOType.OBJECT, entity.getClass());
        objectDao.create(entity);
    }

    /**
     * Changes the existing entity in db.
     *
     * @param entity new entity which should will placed on the old one place.
     * @throws IOException  when had some troubles with DAO connection pool.
     * @throws SQLException when had some troubles with DB interaction.
     */
    public void updateObject(DBEntity entity)
            throws IOException, SQLException {
        objectDao = (ObjectDAO) daoFactory.getDAO(DAOFactory.DAOType.OBJECT, entity.getClass());
        objectDao.update(entity);
    }

    /**
     * Deletes the object from db with specified id.
     *
     * @param clazz
     * @param id
     * @throws ClassNotFoundException if trying to get the not existing entity.
     * @throws IOException            when had some troubles with DAO connection pool.
     * @throws SQLException           when had some troubles with DB interaction.
     */
    public void deleteObject(String clazz, String id)
            throws ClassNotFoundException, SQLException, IOException {
        ((ObjectDAO) daoFactory.getDAO(DAOFactory.DAOType.OBJECT, Class.forName(PREFIX + clazz))).delete(id);
    }
}
