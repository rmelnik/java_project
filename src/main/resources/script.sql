DROP TABLE IF EXISTS CAT;
DROP TABLE IF EXISTS COLLECTIONS;

CREATE TABLE IF NOT EXISTS COLLECTIONS (
  `name`       VARCHAR(255) PRIMARY KEY,
  `type`       VARCHAR(3)   NOT NULL,
  `maxCount`   INTEGER      NOT NULL,
  `jsonSchema` VARCHAR(500) NOT NULL
);

CREATE TABLE IF NOT EXISTS Cat (
  `id`    VARCHAR(255) PRIMARY KEY,
  `value` VARCHAR(255) NOT NULL
);
CREATE TABLE IF NOT EXISTS Dog (
  `id`    VARCHAR(255) PRIMARY KEY,
  `value` VARCHAR(255) NOT NULL
);

INSERT INTO COLLECTIONS VALUES ('Cat', 'LRU', 3,
                                '{"type": "object","properties":{"id":{"type": "string"},"value": {"type": "string"}},"required": ["value"]}');
INSERT INTO COLLECTIONS VALUES ('Dog', 'LFU', 3,
                                '{"type": "object","properties":{"id":{"type": "string"},"value": {"type": "string"}},"required": ["value"]}');

INSERT INTO Cat VALUES ('1', 'Barsik');
INSERT INTO Cat VALUES ('2', 'Murka');
INSERT INTO Cat VALUES ('3', 'Boris');
INSERT INTO Cat VALUES ('4', 'Kitty');
INSERT INTO Cat VALUES ('5', 'Vaska');
INSERT INTO Cat VALUES ('6', 'Sima');
INSERT INTO Cat VALUES ('7', 'Marsik');
INSERT INTO Cat VALUES ('8', 'Sonya');
INSERT INTO Cat VALUES ('9', 'Juja');
INSERT INTO Cat VALUES ('10', 'Kuzya');